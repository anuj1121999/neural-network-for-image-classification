Neural network for Image Classification
Created a 4-layered neural network from scratch to classify images as cats vs non-cats.
Implemented forward, backward propagation and gradient descent from scratch using python.
The classifier has a test accuracy of 80%. 